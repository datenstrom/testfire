#[cfg(test)]
mod tests {

    extern crate arrayfire as af;

    use self::af::*;

    #[test]
    fn it_works1() {
        set_device(0);
        info();
        assert_eq!(2, 2);
    }

    #[test]
    fn it_works2() {
        set_device(0);
        info();
        assert_eq!(2, 2);
    }
    
    #[test]
    fn it_works3() {
        set_device(0);
        info();
        let num_rows: u64 = 5;
        let num_cols: u64 = 3;
        let dims = Dim4::new(&[num_rows, num_cols, 1, 1]);
        let a = constant::<f32>(1.0,dims);
        af_print!("Create a 5-by-3 matrix of random floats on the GPU", a);
        assert_eq!(2,2);
    }

    #[test]
    fn it_works4() {
        set_device(0);
        info();
        let num_rows: u64 = 5;
        let num_cols: u64 = 3;
        let dims = Dim4::new(&[num_rows, num_cols, 1, 1]);
        let a = constant::<f32>(1.0,dims);
        af_print!("Create a 5-by-3 matrix of random floats on the GPU", a);
        assert_eq!(2,2);
    }
}
