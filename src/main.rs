extern crate arrayfire as af;

fn main() {
    af_intro();
    //af_moddims();
    //af_flat();
    //af_flip();
}

fn af_intro() {
    // Create Array with uninitialized values.
    let garbage_vals = af::Array::<f32>::new_empty(af::Dim4::new(&[3, 1, 1, 1]));
    af::print(&garbage_vals);

    // Create an array filled with constant value of `2.0` of type `f32`
    // The type of `Array` is infered from the type of the constant argument
    let const_array = af::constant(2_f32, af::Dim4::new(&[5, 5, 1, 1]));
    af::print(&const_array);

    println!("Create a 5-by-3 matrix of random floats on the GPU");
    let dims = af::Dim4::new(&[5, 3, 1, 1]);
    let a = af::randu::<f32>(dims);
    af::print(&a);

    let values: [u32; 3] = [1u32, 2, 3];
    let indices = af::Array::new(&values, af::Dim4::new(&[3, 1, 1, 1]));
    af::print(&indices);
}

fn af_moddims() {
    println!("Original Array (8, 1, 1, 1):");
    let dims = af::Dim4::new(&[8, 1, 1, 1]);
    let array = af::randu::<f32>(dims);
    af::print(&array);

    println!("Modify Dimensions (2, 4, 1, 1):");
    let new_dims = af::Dim4::new(&[2, 4, 1, 1]);
    let modded = af::moddims(&array, new_dims);
    af::print(&modded);

    println!("Modify Dimensions back to (8, 1, 1, 1) [flatten]:");
    let out = af::moddims(&array, af::Dim4::new(&[array.elements() as u64, 1, 1, 1]));
    af::print(&out);
}

fn af_flat() {
    println!("Original Array (3, 3, 1, 1):");
    let dims = af::Dim4::new(&[3, 3, 1, 1]);
    let array = af::randu::<f32>(dims);
    af::print(&array);

    println!("Flattened Array (9, 1, 1, 1):");
    af::print(&af::flat(&array));
}

fn af_flip() {
    println!("Original Array (5, 2, 1, 1):");
    let dims = af::Dim4::new(&[5, 2, 1, 1]);
    let array = af::randu::<f32>(dims);
    af::print(&array);

    println!("Fliped along the zeroth axes:");
    af::print(&af::flip(&array, 0));

    println!("Fliped along the first axes:");
    af::print(&af::flip(&array, 1));
}
